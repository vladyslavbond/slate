/* file:///usr/share/doc/libglfw3-dev/html/build_guide.html#build_macros */
#include <epoxy/egl.h>
#include <epoxy/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <assert.h>
#include <err.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

const int SLATE_ERROR_UNKNOWN = 500;
const int SLATE_ERROR_GLFW = 501;
const int SLATE_ERROR_GL = 502;
const int SLATE_NOERROR = 0;

void slate_desktop_glfw_error_callback(int error_code, const char *trace)
{
	assert (error_code != 0);
	warn(trace);
}

void slate_desktop_glfw_window_size_callback(GLFWwindow *window_p, int width,
					     int height)
{
	assert (window_p != NULL);
	glViewport(0, 0, width, height);
}

void slate_desktop_gl_check_version(void)
{
	/* FIXME `epoxy_gl_version` always fails at runtime and returns "0". */
}

void slate_desktop_glfw_check_version(void)
{
	int major;
	int minor;
	int rev;
	glfwGetVersion(&major, &minor, &rev);
	assert(3 == major);
	assert(3 <= minor);
	assert(2 <= rev);
}

bool slate_desktop_glfw_vidmode_pref_get(GLFWmonitor *monitor_p, int *width,
					 int *height, int *refresh_rate)
{
	assert (monitor_p != NULL);

	int modes_size;
	const GLFWvidmode *modes_list =
		glfwGetVideoModes(monitor_p, &modes_size);
	assert(modes_size > 0);
	int k = 0;
	int p = 0;
	if (modes_size > 1024) {
		modes_size = 1024;
		warn(u8"list of available video modes is too large");
	}
	while (k < modes_size) {
		if (modes_list[k].width > modes_list[p].width ||
		    modes_list[k].height > modes_list[p].height) {
			p = k;
		}
		++k;
	}

	if (GLFW_NO_ERROR == glfwGetError(NULL)) {
		*width = modes_list[p].width;
		*height = modes_list[p].height;
		*refresh_rate = modes_list[p].refreshRate;
		return true;
	} else {
		return false;
	}
}

GLFWwindow *slate_desktop_glfw_window_get(void)
{
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);
	glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
	glfwWindowHint(GLFW_FOCUSED, GLFW_TRUE);
	glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_FALSE);
	glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);
	glfwWindowHint(GLFW_CENTER_CURSOR, GLFW_FALSE);
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_FALSE);
	glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_TRUE);
	glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_FALSE);

	glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GLFW_FALSE);

	GLFWmonitor *monitor_p = glfwGetPrimaryMonitor();
	int width = 1024;
	int height = 768;
	int refresh_rate = 56;
	slate_desktop_glfw_vidmode_pref_get(monitor_p, &width, &height,
					    &refresh_rate);
	glfwWindowHint(GLFW_REFRESH_RATE, refresh_rate);
	GLFWwindow *window_p =
		glfwCreateWindow(width, height, u8"slate", monitor_p, NULL);
	glfwSetWindowSizeLimits(window_p, 640, 480, 1920, 1080);
	glfwSetWindowAspectRatio(window_p, 4, 3);
	glfwMakeContextCurrent(window_p);
	glfwSwapInterval(1);
	glfwSetFramebufferSizeCallback(window_p,
				       slate_desktop_glfw_window_size_callback);
	return window_p;
}

int slate_desktop_gl_render(void)
{
	glClearColor(0.5f, 0.5f, 0.2f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	float data[] = {
		1.0f,  1.0f,  0.0f, -1.0f, 1.0f,  0.0f,
		-1.0f, -1.0f, 0.0f, 1.0f,  -1.0f, 0.0f,
	};
	unsigned int vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);

	return glGetError();
}

int slate_desktop_begin(GLFWwindow *window_p)
{
	assert(NULL != window_p);
	glfwMakeContextCurrent(window_p);

	/* TODO Remove reliance on clock reset. Store all values in scope. */
	int exitval = SLATE_NOERROR;

	glfwSetTime(0.0);
	const double delay_duration = 1.0 / 12.0;
	while (!glfwWindowShouldClose(window_p)) {
		double duration_elapsed = glfwGetTime();
		if (duration_elapsed >= delay_duration) {
			const int r = slate_desktop_gl_render();
			if (GL_NO_ERROR != r) {
				/* Break the loop implicitly. */
				glfwSetWindowShouldClose(window_p, GLFW_TRUE);
				exitval = SLATE_ERROR_GL;
				warn(u8"GL error");
			}

			glfwSwapBuffers(window_p);
			glfwPollEvents();

			glfwSetTime(0.0);

			const char **desc = NULL;
			const int e = glfwGetError(desc);
			if (GLFW_NO_ERROR != e) {
				/* Break the loop implicitly. */
				glfwSetWindowShouldClose(window_p, GLFW_TRUE);
				exitval = SLATE_ERROR_GLFW;
				warn(*desc);
			}
		}
	}

	glfwDestroyWindow(window_p);

	return exitval;
}

void slate_desktop_gl_image_load(const char* pathname, uint8_t *buf_p)
{
	assert(pathname != NULL);
	assert(buf_p != NULL);
	printf("image_load %s\n", pathname);

	int32_t errcode = slate_xz_open();
	assert(0 == errcode);

	int32_t size = 0;
	errcode = slate_xz_apply(pathname, buf_p, &size);
	printf("slate_xz_apply: %d\n", errcode);

	slate_xz_close();
	//assert(0 == errcode);

	printf("%s size: %d", pathname, size);
}

int main(int argc, char** argv)
{
	assert(argc >= 1);

	int exitval = SLATE_NOERROR;

	slate_desktop_glfw_check_version();
	slate_desktop_gl_check_version();

	glfwSetErrorCallback(slate_desktop_glfw_error_callback);

	int i = glfwInit();
	if (GLFW_FALSE == i) {
		warn(u8"could not initialize GLFW");
		exitval = SLATE_ERROR_GLFW;
		goto close;
	}

	GLFWwindow *window_p = slate_desktop_glfw_window_get();
	if (NULL == window_p) {
		warn(u8"could not create GLFW window");
		exitval = SLATE_ERROR_GLFW;
		goto close;
	}

	/* TODO Iterate over all CLI argumnets. */
	const char pathname[8192] = {0};
	const char r = realpath(argv[1]);
	assert(r != NULL);
	assert (pathname == r);
	uint8_t buf_p[65535];
	slate_desktop_gl_image_load(pathname, buf_p);

	int j = slate_desktop_begin(window_p);
	if (SLATE_NOERROR != j) {
		exitval = j;
	}

	goto close;

close : {
	glfwTerminate();
	return exitval;
}
}
