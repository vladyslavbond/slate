#include <assert.h>
#include <lzma.h>
#include <stdbool.h>
#include <stdio.h>
#include "slate/internal/xz.h"

static lzma_stream *stream_p;

void slate_xz_warn(lzma_ret r)
{
	printf("yoh\n");
	switch (r) {
		case (LZMA_OK): {
			printf(u8"xz: latest operation successful\n");
			break;
		}
		case (LZMA_STREAM_END): {
			printf(u8"xz: reached end of stream\n");
			break;
		}
		case (LZMA_UNSUPPORTED_CHECK): {
			printf(u8"xz: cannot validate integrity check\n");
			break;
		}
		case (LZMA_MEM_ERROR): {
			printf(u8"xz: memory allocation error\n");
			break;
		}
		case (LZMA_MEMLIMIT_ERROR): {
			printf(u8"xz: given memory constraint violated\n");
			break;
		}
		case (LZMA_OPTIONS_ERROR): {
			printf(u8"xz: unsupported codec options\n");
			break;
		}
		case (LZMA_PROG_ERROR): {
			printf(u8"xz: illegal state\n");
			break;
		}
		default: {
			printf(u8"xz: unknown liblzma error\n");
		}
	}
}

int32_t slate_xz_open(void)
{
	if (stream_p != NULL) {
		return 513;
	}

	lzma_stream strm = LZMA_STREAM_INIT;
	stream_p = &strm;

	lzma_mt options = {0};

	options.flags = LZMA_FAIL_FAST | LZMA_TELL_ANY_CHECK | LZMA_TELL_NO_CHECK;

	const uint32_t tmax = lzma_cputhreads();
	uint32_t t = 1;
	if (tmax > 1) {
		t = tmax - 1;
	}
	options.threads = t;

	options.timeout = 30000;

	options.memlimit_stop = 65535;
	options.memlimit_threading = 65535;

	const lzma_ret errcode = lzma_stream_decoder_mt(stream_p, &options);
	printf("check decoder init\n");
	slate_xz_warn(errcode);
	if (LZMA_OK != errcode) {
		lzma_end(stream_p);
		stream_p = NULL;
		return 514;
	} else {
		return 0;
	}
}

lzma_check slate_xz_integrity_check(void)
{
	assert(stream_p != NULL);

	const lzma_check c = lzma_get_check(stream_p);
	if (LZMA_CHECK_NONE == c) {
		printf("xz: cannot decode *.xz files without any integrity checks");
	}

	if (!lzma_check_is_supported(c)) {
		printf("xz: cannot perform this particular integrity check");
	}

	switch (c) {
		case (LZMA_CHECK_CRC32): {
			printf("xz: crc32 not supported yet");
		}
		case (LZMA_CHECK_CRC64): {
			printf("xz: crc64 not supported yet");
		}
		case (LZMA_CHECK_SHA256): {
			printf("xz: sha256 not supported yet");
		}
		default: {
			printf("xz: unknown error during integrity check %d", c);
		}
	}

	return c;
}

int32_t slate_xz_apply(const char *pathname, uint8_t *outbuf_p, int32_t *outsize_p)
{
	assert (pathname != NULL);
	assert (outbuf_p != NULL);

	FILE* connection_p = fopen(pathname, "r");
	if (NULL == connection_p) {
		return 500;
	}

	uint8_t buf_p[65535];
	const size_t nmemb = sizeof(buf_p);
	/* WARNING: `size` must always be equal to 1. */
	const size_t size = 1;

	stream_p->avail_in = 0;
	stream_p->avail_out = sizeof(outbuf_p);
	stream_p->next_in = buf_p;
	stream_p->next_out = outbuf_p;

	*outsize_p = 0;

	int32_t exitcode = 501;

	printf("run\n");
	while (!feof(connection_p)) {
		/* NOTE I don't understand what this condition actually implies or why is it required. */
		if (0 == stream_p->avail_in) {
			stream_p->avail_in = fread(buf_p, size, nmemb, connection_p);
			*outsize_p += stream_p->avail_in;
		}
		if (ferror(connection_p) != 0) {
			printf("xz: could not read input file");
			exitcode = 502;
			break;
		}
		printf("yok\n");
		const lzma_ret ret = lzma_code(stream_p, LZMA_RUN);
		printf("ret: %d\n", ret);
		if (LZMA_GET_CHECK == ret || LZMA_UNSUPPORTED_CHECK == ret || LZMA_NO_CHECK == ret) {
			printf("xz: integrity check\n");
			slate_xz_integrity_check();
		}
		slate_xz_warn(ret);
		if (LZMA_STREAM_END == ret) {
			exitcode = 0;
			printf("decoded all\n");
			break;
		} else if (LZMA_OK != ret) {
			exitcode = 600;
			printf("xz error\n");
			break;
		}
		if (0 == feof(connection_p)) {
			printf("feof\n");
			break;
		}
	}
	printf("stop\n");

	const lzma_ret r = lzma_code(stream_p, LZMA_FINISH);
	if (LZMA_GET_CHECK == r || LZMA_UNSUPPORTED_CHECK == r || LZMA_NO_CHECK == r) {
		printf("xz: integrity check again??\n");
		slate_xz_integrity_check();
	}
	slate_xz_warn(r);
	if (LZMA_STREAM_END != r) {
		printf("weirder xz error: %d\n", r);
		exitcode = 700;
	}
	fclose(connection_p);

	return exitcode;
}

void slate_xz_close(void)
{
	if (NULL == stream_p) {
		return;
	}

	lzma_end(stream_p);
	stream_p = NULL;
}
