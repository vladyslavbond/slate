#include <assert.h>
#include <err.h>
#include <stdint.h>
#include <stdio.h>

#include <lzma.h>

void cura_lzma_init(lzma_stream* stream_p)
{
	lzma_stream strm = LZMA_STREAM_INIT;
	stream_p = &strm;

	lzma_mt options = {0};

	const uint32_t tmax = lzma_cputhreads();
	uint32_t t = 1;
	if (tmax > 1) {
		t = tmax - 1;
	}
	/* It appears that options.threads is the only mandatory field. */
	options.threads = t;

	const lzma_ret errcode = lzma_stream_decoder_mt(stream_p, &options);
	switch (errcode) {
		case (LZMA_OK): {
			warn("init successful\n");
			break;
		}
		case (LZMA_OPTIONS_ERROR): {
			err(errcode, "could not parse options");
			break;
		}
		default: {
			 err(errcode, "could not init lzma");
		}
	}
}

int main(int argc, char** argv)
{
	assert (argc >= 1);

	FILE* swimsuit_p = fopen(argv[1], "r");
	if (NULL == swimsuit_p) {
		err(2, "could not read file");
	}

	uint8_t buf_p[BUFSIZ] = {0};
	uint8_t outbuf_p[BUFSIZ] = {0};



	lzma_stream strm = LZMA_STREAM_INIT;
	lzma_stream* stream_p = &strm;

	lzma_mt options = {0};

	const uint32_t tmax = lzma_cputhreads();
	uint32_t t = 1;
	if (tmax > 1) {
		t = tmax - 1;
	}
	/* It appears that options.threads is the only mandatory field. */
	options.threads = t;

	options.memlimit_threading = 8192 * 8192 * 2 + 16;
	options.memlimit_stop = 8192 * 8192 * 2 + 16;

	const lzma_ret errcode = lzma_stream_decoder_mt(stream_p, &options);
	switch (errcode) {
		case (LZMA_OK): {
			warn("init successful\n");
			break;
		}
		case (LZMA_OPTIONS_ERROR): {
			err(errcode, "could not parse options");
			break;
		}
		default: {
			 err(errcode, "could not init lzma");
		}
	}

	assert(stream_p != NULL);

	stream_p->avail_in = 0;
	stream_p->next_in = buf_p;

	stream_p->avail_out = sizeof(outbuf_p);
	stream_p->next_out = outbuf_p;

	size_t b;
	lzma_ret r;
	while (1) {
		/* Nothing to read, give it something to read. */
		if (0 == stream_p->avail_in && !feof(swimsuit_p)) {
			stream_p->next_in = buf_p;
			stream_p->avail_in = fread(buf_p, 1, sizeof(buf_p), swimsuit_p);
		}

		r = lzma_code(stream_p, LZMA_RUN);

		switch (r) {
			case (LZMA_OK): {
				break;
			}
			case (LZMA_STREAM_END): {
				warn("LZMA_STREAM_END");
				break;
			}
			default: {
				warn("r: %d", r);
				err(r, "lzma error");
			}
		}

		/* Wrote everything out, so time to write some more?? */
		if (0 == stream_p->avail_out || LZMA_STREAM_END == r) {
			b = fwrite(outbuf_p, 1, sizeof(outbuf_p) - stream_p->avail_out, stdout);
			stream_p->avail_out = sizeof(outbuf_p);
			stream_p->next_out = outbuf_p;
		}

		if (ferror(swimsuit_p)) {
			err(2, "ferror");
		}

		if (LZMA_STREAM_END == r) {
			break;
		}
	}

	fclose(swimsuit_p);
	lzma_end(stream_p);

	return 0;
}
