# slate

Student project. The goal is to learn C11, GL33C and surrounding ecosystem.
Presently it's only a collection of code snippets.

## Dependencies

See `debian/control` or `etc/cc.txt` for dependencies.

## Compile

By default the executable is written to `target/slate`. Override it with `-o`
option.

### GCC

``` sh
gcc @etc/cc.txt -o target/slate
```

### CLang

``` sh
clang @etc/cc.txt -o target/slate
```

### CompCert

CompCert compiler requires additional library `--library=compcert` to build
anything. It is included together with the rest of their project under
`runtime/libcompcert.a`.

``` sh
ccomp -v @etc/cc.txt -L${PATH_TO_COMPCERT_BASEDIR}/runtime -o target/slate
```

## Build

Build an installable `*.deb` package for Debian 12 (bookworm).

``` sh
dpkg-buildpackage -b
```

## Install

After the Debian binary package (`*.deb`) is built. Adjust the command for the
project version and the platform architecture. Requires superuser privilege.

``` sh
dpkg-deb -i target/slate_0.0.1_amd64.deb
```
