int32_t slate_xz_open(void);

int32_t slate_xz_apply(const char *pathname, uint8_t *buf_p, int32_t *outsize);

void slate_xz_close(void);
